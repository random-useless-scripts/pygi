print ("Content-Type: text/html")
print ()

indent_level = 0
tag_stack = []

def start(tag, newline=True, indent=True, *args, **items):
  str_build = ' '*indent_level
  str_build += '<' + tag
  for a in args:
    str_build += ' ' + a
  for k, v in items.items():
    str_build += ' ' + k + '="' + v + '"'
  str_build += '>'
  if indent:
    global indent_level
    indent_level += 1
  if newline:
    print (str_build)
  else:
    print (str_build, end='')
  global tag_stack
  tag_stack.append(tag)

def end(tag=None, newline=True, indent=True):
  if tag is None:
    tag = tag_stack[-1]
    del(tag_stack[-1])
  str_build=str()
  if indent:
    global indent_level
    indent_level -= 1
    str_build = ' '*indent_level
  str_build += '</' + tag + '>'
  if newline:
    print (str_build)
  else:
    print (str_build, end='')

def endall():
  global tag_stack
  while tag_stack:
    end()

def script(text, language="text/javascript"):
  start("script", type=language)
  print(text)
  end()
  

def single(tag, *args, **items):
  """
   Doesn't modify the tag_stack or change indent_level
  """
  str_build = ' '*indent_level
  str_build += '<' + tag
  for a in args:
    str_build += ' ' + a
  for k, v in items.items():
    str_build += ' ' + k + '="' + v + '"'
  str_build += ' />'
  print (str_build)

def h1(text):
  start("h1", newline=False, indent=False)
  print (text, end='')
  end("h1", indent=False)
