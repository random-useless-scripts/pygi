#!/usr/bin/python3
from pygi import *

from cgi import FieldStorage
import json

try:
  fhistory = open("chathistory.json", mode='r')
  history = json.loads(fhistory.read())
  fhistory.close()
except:
  history = {}

form = FieldStorage()
if 'newMessage' in form :
  import datetime
  currentDateTime = datetime.datetime.ctime(datetime.datetime.now())
  newMessage = form['newMessage'].value
  history[currentDateTime] = newMessage
  fhistory = open("chathistory.json", mode='w')
  fhistory.write(json.dumps(history))
  fhistory.close()


start("html")
start("head")
single("meta", 'http-equiv="refresh" content="0;chat.py"')
endall()
