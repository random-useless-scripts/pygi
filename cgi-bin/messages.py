#!/usr/bin/python3
from pygi import *
import json

try:
  fhistory = open("chathistory.json", mode='r')
  history = json.loads(fhistory.read())
  fhistory.close()
except:
  history = {}
  
start("html")
start("head")
#single("meta", 'http-equiv="refresh" content="1"')
end("head")

start("body", bgcolor='lightblue')
start("div")
#order history
for dt, m in sorted(history.items()):
  start("p")
  print ("<b>%s:</b> %s"%(dt, m))
  end()

endall()
